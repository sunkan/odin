define('Odin/bind', ['Odin/index', 'Buri/util', 'Buri/dom'], function(Odin, util, dom) {
  Odin.registerAttr('data-bind', function(el, data, tpl) {
    var bind = util.parseData(el.getAttribute('data-bind'));
    el.removeAttribute('data-bind');
    for (var key in bind) {
      var attr = 'html',value, old;
      if (bind.hasOwnProperty(key)) {
        if (!util.isInt(key)) {
          attr = key;
        } else if (el.tagName.toLowerCase()=='input') {
          attr = 'value';
        }
        value = tpl.get(bind[key], data);
        if ( (old = dom.get(el, attr)) && old.indexOf('{')) {
          var td = {};
          td[(bind[key].split('.').pop())] = value;
          value = util.template(old, td);
        }
        dom.set(el, attr, value);
      }
    }
    return el;
  });
});