define('Odin/moment', ['Odin/index', 'Buri/util', 'Buri/dom', 'moment'], function(Odin, util, dom, moment) {
  "use strict";
  Odin.registerAttr('data-bind-moment', (function() {
    var 
      momentEls = [],
      doUpdate = false,
      updateRs,
      handleMoment = function(el, value) {
        var format = el.getAttribute('data-moment'),
          m = util.isInt(value)?moment.unix(value):moment(value);
        if (format === 'LLL') {
          if (moment().diff(m, 'days') < 1) {
            format = 'fromNow';
          }
        }
        if (m[format]) {
          value = m[format]();
        } else {
          value = m.format(format);
        }
        return value;
      },
      update = function() {
        momentEls.forEach(function(row) {
          dom.set(row.el, row.attr, handleMoment(row.el, row.value));
        });
      };

    return function(el, data, tpl) {
      var bind = util.parseData(el.getAttribute('data-bind-moment')), key;
      el.removeAttribute('data-bind-moment');
      util.forIn(bind, function(row, key) {
        var attr = 'html',value, old;
        if (!util.isInt(key)) {
          attr = key;
        } else if (el.tagName.toLowerCase()=='input') {
          attr = 'value';
        }
        value = handleMoment(el, tpl.get(row, data));
        if (el.hasAttribute('data-moment-update')) {
          momentEls.push({el:el, value:value, attr:attr});
          doUpdate = true;
        }
        if ( (old = dom.get(el, attr)) && old.indexOf('{')) {
          var td = {};
          td[(row.split('.').pop())] = value;
          value = util.template(old, td);
        }
        dom.set(el, attr, value);
      });
      if (!updateRs && doUpdate) {
        updateRs = setInterval(update, 60000);
      }
      return el;
    };
  })());
});