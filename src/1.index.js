/*
 * Build date: @DATE
 * Build version: @VERSION
 * Author: @AUTHOR
 */
define('Odin/index', ['Buri/util', 'Buri/dom'], function(util, dom) {
  var 
    objectGet = util.objectGet,
    attrClbs = [],
    parseTpl = function(el, data, tplObj) {
      for (var index = 0, length = attrClbs.length; index < length; index++) {
        var els = dom.selectAll('['+attrClbs[index].attr+']', el);
        if (els && els.length) {
          for (var elementIndex = 0, elementLength = els.length; elementIndex < elementLength; elementIndex++) {
            attrClbs[index].clb(els[elementIndex], data, tplObj);
          }
        }
      }
      return el;
    },
    obj = function(el) {
      if (el && !el.nodeType) {
        el = util.stringToElement(el);
      }
      this.el = el;
    };
  obj.registerAttr = function(attr, clb) {
    attrClbs.push({
      attr: attr, 
      clb: clb
    });
  };
  obj.prototype.get = function(key, data) {
    return objectGet(key, data);
  };
  obj.prototype.render = function(el, data) {
    if (!data) {
      data = el;
      el = this.el;
    }
    return parseTpl(el, data, this);
  };
  return obj;
});