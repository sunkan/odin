define('Odin/if', ['Odin/index'], function(Odin) {
  Odin.registerAttr('data-bind-if', function(el, data, tpl) {
    var _if = el.getAttribute('data-bind-if'),
      rs = false,
      expr = _if.indexOf('&&')?_if.split('&&'):[_if];
    el.removeAttribute('data-bind-if');

    for(var i = 0, l = expr.length; i < l; i++) {
      rs = false;
      if (expr[i].substr(0,1)=='!') {
        if (!tpl.get(expr[i].substr(1).trim(), data)) {
          rs = true;
        }
      } else if (expr[i].indexOf('>')!==-1) {
        var parts = expr[i].split('>');
        if (tpl.get(parts[0].trim(), data) > parseInt(parts[1])) {
          rs = true;
        }
      } else if (tpl.get(expr[i], data)) {
        rs = true;
      }
      if (!rs) {
        break;
      }
    }
    !rs && el.parentNode.removeChild(el);
  });
});