define('Buri/promise',[], function(){
  "use strict";
  return function() {
    var callbacks = [],
      resolve = function (result) {
        complete('resolve', result);
      },
      reject = function(err) {
        complete('reject', err);
      },
      then = function(resolve, reject) {
        callbacks.push({ resolve: resolve, reject: reject });
      },
      promise = {
        resolve: resolve,
        reject: reject,
        then: then
      };
     
    function complete(type, result) {
      promise.then = type === 'reject' ? function(resolve, reject) { 
        reject && reject(result); 
      } : function(resolve) { 
        resolve && resolve(result); 
      };
      promise.resolve = promise.reject = function() { throw new Error("Promise already completed"); };
      var i = 0, cb;
      /* jshint -W084 */
      while(cb = callbacks[i++]) { 
        cb[type] && cb[type](result); 
      }
      callbacks = null;
    }
     
    return promise;
  };
});